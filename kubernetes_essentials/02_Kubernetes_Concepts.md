## Containers and Pods
---
- Pods are the smallest and most basic building block of the kubernetes model
- Pods are usually a single container but can contain multiple containers sharing storage resource and a unique IP address in the Kubernetes cluster network
- In order to run containers Kubernetes schedules pods to run on server (nodes) 

## Clusterin and Nodes
---
- By being deployed to a cluster K8's is deployed across many different server
- Generally split into Controller nodes and worker nodes
- Can have more than one controller node for high availability
- Controller nodes run the Kubernetes API, worker nodes run the applications

```
kubectl get nodes (shows nodes running in the cluster)

kubectl describe node {node_name} (shows details of a specific named node)
``` 

## Networking
---
- The K8's networking model involves creating a virtual network across the whole cluster. This virtual network is seperate from the physical network and spans the entire cluster.
- This means that every pod on the cluster has a unique IP address and can communicate with any other pod in the cluster even if that other pod if on a different node.
- Create two nginx pods and a busybox pod
```
cat << EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.15.4
        ports:
        - containerPort: 80
EOF

cat << EOF | kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  name: busybox
spec:
  containers:
  - name: busybox
    image: radial/busyboxplus:curl
    args:
    - sleep
    - "1000"
EOF

kubectl exec busybox -- curl {id-address of pod}
```

## Kubernetes Architecture
---
- K8's is made of many services which come together to give the functionality of a K8's cluster. 
- With kubeadm many of these componenets are run as pod within the cluster.

- The control plane is made up of
  - **etcd**: provides distributed data storage for the cluster state
  - **kube-apiserver**: serves the kubernetes API the primary interface for the cluster
  - **kube-controller-manager**: Bundles several backenb components into one package.
  - **kube-scheduler**: Schedules pods to run on individual nodes
- Each node has
  - **kubelet**: Agent the executes containers on each node
  - **kube-proxy**: Handles network communication between nodes by adding firewall routing rules
- 