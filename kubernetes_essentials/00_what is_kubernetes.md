# What is Kubernetes
Kubernetes is a production grade container orchestration tool.

- Cotainers wrap software in independant portable packages making it easy to quickly run software in a variety of environments.
- With containers you run a variety of software components across a cluster of generic servers.
- You can run redundant services across these services and stand up new copies of these software components if copies fail.

## What problems does Kubernetes solve
- How do you ensure that multiple instances of a piece of software are spread across multiple servers for high availability
- How to deploy new code changes and roll them out across the cluster
- How can I create new containers to handle additional load (scale up)