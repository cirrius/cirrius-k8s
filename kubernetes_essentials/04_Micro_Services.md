# MicroServices

## What are MicroServices
---
- Micro Services break up a complete monolithic system into small independant services that work together to form a whole.
- Many application are desgined with monolithic architecture meaning that all parts of the application are combined in one large executable.
- This can allow you to scale different elements of your application or even use completely different programming languages for each element
- Advantages of micro services
  - Scalability - individual elements are independantly scalalbe from the whole application
  - Cleaner code - when services are relatively independant it is easier to make a chjange in one area of the application without breaking things in other areas
  - Reliability - problems in one area of the application are less likely to affect other areas
  - Variety of tools - different parts of the application can be built with the best tool for the job.

## Stans Robot Shop
---
- https://github.com/linuxacademy/robot-shop is an example micro service application made from many different languages
- This can be deployed to a cluster via the following
```
cd ~/
git clone https://github.com/linuxacademy/robot-shop.git

(Create this application within it's own namespace as it will created many pods)
kubectl create namespace robot-shop
kubectl -n robot-shop create -f ~/robot-shop/K8s/descriptors/

(use specific name space )
kubectl get pods -n robot-shop -w

(the service is now available on the public ip of the cluster it is running on)
http://${kube_server_public_ip}:30080

(edit the number of mongodb pods)
kubectl edit deployment mongodb -n robot-shop
```
- 