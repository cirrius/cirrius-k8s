# Installing Kubernetes

https://kubernetes.io/docs/setup/production-environment/container-runtimes/
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

## Installing Docker
---
- Add the docker repo GPG key
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
- Add the Docker repostiory
```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
- Update the apt sources list
```
sudo apt-get update
```
- Install docker-ce
```
sudo apt-get install -y docker-ce=18.06.1~ce~3-0~ubuntu
```
- Prevent auto-updates for the Docker package
```
sudo apt-mark hold docker-ce
```
- Configure docker cgroupdriver to be systemd to match Kubernetes
```
# Set up the Docker daemon
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

sudo mkdir -p /etc/systemd/system/docker.service.d

# Restart Docker
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl enable docker

# Check all the above was succesful: `Cgroup Driver: systemd`
sudo systemctl enable docker
```

## Installing Kubernetes
---
1. Kubeadm is a tool which automates a large portion of setting up the cluster
2. Kubelet handles running containers on a node
3. Kubectl is a command line tool for interacting with the cluster
<br></br>
- Add the K8s repo GPG key
```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```
- 
```
cat << EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
```
- Update the apt sources list
```
sudo apt-get update
```
- Install Kubernetes applications
```
sudo apt-get install -y kubelet=1.15.7-00 kubeadm=1.15.7-00 kubectl=1.15.7-00
```
- Prevent auto-updates for the Docker package
```
sudo apt-mark hold kubelet kubeadm kubectl
```
- Deactivate swap - only works for current session
```
swapoff -a
```
- Deactivate Swap on restart (**use this one**)
```
$ vi /etc/fstab
#comment out line with swap
```  

## Bootstrap the cluster
---
- Initiate the master node, the pod-network-cidr flag needs to be passed in for the network flannel plugin
```
kubeadm init --pod-network-cidr=10.244.0.0/16
```
- Copy the commands given to setup local kubeconfig
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Add the follwing to the .bashrc file to esnure the cluster config can be accessed
export KUBECONFIG="$HOME/.kube/config"
```
- Verify the cluster is working, look for Client and Server versions
```
kubectl version
```
- Run the kubeadm join command on the worker nodes
```
sudo kubeadm join $some_ip:6443 --token $some_token --discovery-token-ca-cert-hash $some_hash
```
- Verify that all nodes have succesfully joined the cluster
```
kubectl get nodes
```

## Setup Flannel Network Layer
---
- On all nodes run the following commands which will turn on ip-tables
```
echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```
- On the Kubernetes Master apply the Network configuration using kubectl
```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
- Verify that the nodes are ready
```
kubectl get nodes
```
- Verify the flannel pods are running, look for threee pods with flannel in the name
```
kubectl get pods -n kube-system
```