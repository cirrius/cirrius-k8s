# Deployments and Services

## Deployments
---
- Deployments allow you to specify a desired state for a series of pods. The cluster will then constantly work to maintain that desired state.
- This can be used for:
  - Scaling: You can specify the number of replicas you want and the deployment will create or remove pods to meet that requirement.
  - Rolling Updates: With a deployment you can change the deployment container image to a new version of the image. The deployment will gradually replace existing containers with the new version.
  - Self-Healing: If one of the pods in the deployment is accidentally destroyed the deployment will immediately spin up a new one to replace it.
- The following command creates a deployment of 2 replicas of a single nginx pod
```
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.15.4
        ports:
        - containerPort: 80
EOF

kubectl get deployments


kubectl describe deployment nginx-deployment
```

## Services
---
- Services allow you to dynamically access a group of replica pods. Replica pods are often being created and destroyed so if another pod or external entity needs to access those pods it can communicate with a single service.
- A service creates an abstraction layer on top of a set of replica pods. You can access the service rather than accessing the pods.
- This provides essentially a load balancer in front of a deployment.
- The service uses selector to assign pods to a service based on what labels each pod has
```
cat << EOF | kubectl create -f -
kind: Service
apiVersion: v1
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
    nodePort: 30080
  type: NodePort
EOF

kubectl get svc

curl localhost:30080

kubectl exec busybox -- curl -s nginx-service
```
- 