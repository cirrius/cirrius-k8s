# Kuberenetes Deployments

## Concepts
- Deployments manage replica sets
- Deployments define an uypdate strategy and allow for rolling updates, 
- Max unavailable sets the number of pods that can be replaced at any one time
- MinReadySeconds detemines how long a pod must be up before it can be declared good within the context of the rolling update

## Commands
- Start a deployment 
```
kubectl create -f ./deployment_example.yaml
```
- Update the image in a deployment
```
kubectl set image deployment.v1.apps/example-deployment nginx=darealmc/nginx-k8s:v2
kubectl set image deployment.v1.apps/<deployment name> <container name>=<new image name>

kubectl set image deployment.v1.apps/nginx nginx=darealmc/nginx-k8s:v2
```
- Show the history for a deployment
```
kubectl describe deployment <deployment name>
- 