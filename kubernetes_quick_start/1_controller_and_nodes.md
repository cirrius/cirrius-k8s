# Master and Nodes in Kubernettes


## Commands
- view nodes
```
kubectl get nodes
```

- view all pods
```
kubectl get pod --all-namespaces -o wide
```

- Kubernetes is running in docker, this can be viewed by
```
sudo docker ps
```

## Concepts

- The master node is made up a number of pods
    - **Etcd** (pod) an internal databases for the K8's cluster
    - **API Server** (pod) is the front end of the K*'s control plan which processes commands from the rest of the cluster
    - **Scheduler** (pod) watches the other nodes and when a new pod is needed it determines which is the best node to create that pod based on many factors including hardware, workloads, affinity
    - **Controller Manager** (pod) Node, Replication, Endpoints and Service Account & Token Controllers are all controlled by the controller manager
- A worker node is made up of fewer base pods but can scale up to run more pods as the cluster needs
    - **Proxy** (pod) manages if a pod needs to expose a port or port forwarding outside of the cluster
    - **Network Overlay** (pod) manages networking to the rest of the cluster
    - **Kubelet** (daemon) primary node agent that runs on each node. use PodSpec a provided object that describes a pod to monitor the pods on it's node. The kubelet checks the state of its pods and ensures they match the spec.
    - **Container Runtime** (daemon) - Docker in this case but is used to instantiate new containers. Must be compliant with the open container initiative.