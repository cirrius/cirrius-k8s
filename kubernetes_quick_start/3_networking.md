# Networking in Kubernettes

## Concepts
- Kubernetes can use any of a number of container network fabrics
- For the quickstart guide we are using [flannel](https://github.com/coreos/flannel)
- If you need network policy rules you will need a different network fabric than flannel - there is very little post configuration with flannel
- Flannel sets up ip routing tables between all of the nodes for communciation between them
- The network plugin needs to be initialised before the cluster can start as the DNS pod will be unable to resolve a source for IP Addresses
- DNS pods need to be stood up inside the pod network not on the master node
- This is setup so that if a pod is lost a new pod can be stood up very easily and it's addresses quickly communciated to the rest of the cluster. This means the network must be very flexible or elastic in the way IP addresses are assigned
- Other network overlays that could be considered
    - istio
    - calico
    - weavenet

## Comands
- Show all ip routes
```
ip route
```
- Show flannel process running in the master or a node
```
ps -ax | grep [f]lannel
```
