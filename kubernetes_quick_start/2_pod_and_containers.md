# Pods and Containers

## Concepts
- Pods are the smallest unit managable in K8's
- Pods can contain many containers within one pod
    - The containers will all share the same IP address and port range
    - The also can have access to shared volumes between each other
    - They can talk to each other via localhost
- Kubernetes is declartive so the setup of a pod is declared within a yaml - see example `podexample.yaml`
- If a Docker container does not have a command that it is continually running it will exit immediately. This is why the second container in the podexample has an infinite loop

## Commands
- Get all namespacess that are on this cluster
```
kubectl get namespaces
```
- Create a new namespace
```
kubectl create namespace <namespace name>
```
- Create a pod from a yaml file
```
kubectl create -f ./podexample.yaml
```
- Show running pods within a namespace
```
kubectl --namespace=<namespace name> get pods
```
- Delete a running pod
```
kubectl --namespace=<namespace name> delete pod <pod name>
```

