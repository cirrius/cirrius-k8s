# Kubernetes Services

## Concepts
- Services obfuscate the pods that they are in front of
- If the pods get blown up and recreated the service remains and re-direct around that pod
- Declaritively defined in the same way that pods and replica sets are
- Select the pods that will be targeted by the service with the selector
- All pods that match that selector will be reached on the targetPort from the service
- The service name can be looked up from DNS within the cluster / pods
- This is how K8's does high availability and service discovery

## Commands
- Create a new service
```
kubectl create -f ./service-example.yaml
```
- Describe all services
```
kubectl describe service
```
- Describe a specific service
```
kubectl describe service <service name>
```