# Kubernetes DNS

## Concepts
- All services that are defined in the cluster get a DNS record. This is true for the DNS service as well
- Pods search DNS relative to their own namespace
    - if a pod in the namespace `web_data` is looking for an service `database` it will look within it's own namespace to find that
- The DNS Server schedules a DNS pod on the cluster and configures the kubelets to set the containers to use the cluster's DNS service
- PodSpec DNS policies determine the way that a container use DNS. Options include `Default`, `ClusterFirst` or `None`

## Commands
- Start an interactive terminal within a specific pod using a bash shell
```
kubectl exec -it <pod name> /bin/bash
```