# Installing Kubernettes

Goal is to install kubernettes on a controller node and two worker nodes.

# Steps
- On logging into the first server, `sudo su` as all comands will be caried out sudo from here on
- Disable SELinux - s it would take too long to set up run configurations for K8's - **Do not do this in Production**
```
setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```
- Enabled the `br_netfliter` module for cluster communication and set the file `bridge-nf-call-iptables` to `1`
```
modprobe br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```
- Disable swap to prevent memory allocation issues during times of memory stress as swap can incorrectly report swap as real memory
```
swapoff -a
vim /etc/fstab
#comment out line with swap
```  

- Install the prereqs for Docker
```
yum install -y yum-utils device-mapper-persistent-data lvm2
```
- Add the Docker repo and install Docker
```
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
```
- Configure the Docker Cgroup Driver to systemd, enable and start Docker - cgroup driver is used to report on system statistics, if Docker and K8's are on different cgroups they will miss each other and report statistics incorrectly.
```
sed -i '/^ExecStart/ s/$/ --exec-opt native.cgroupdriver=systemd/' /usr/lib/systemd/system/docker.service 
systemctl daemon-reload
systemctl enable docker --now 
systemctl status docker
docker info | grep -i cgroup
```
- Add the Kubernetes repo
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
      https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```
- Install Kubernetes
```
yum install -y kubelet kubeadm kubectl
```
- Enable Kubernetes, this does not completely enable kubernetes the kubeadm init adds more configuration files before the service fully starts
```
systemctl enable kubelet
```

### Complete the following steps on the controller node only
---
- Initialize the cluster using the IP range for Flannel.
```
kubeadm init --pod-network-cidr=10.244.0.0/16
```
- Copy the kubeadm join command - should look something like.
```
kubeadm join <ip address>:<port> --token <token> --discovery-token-ca-cert-hash sha256:<sha>
```
- Exit sudo and run the following as instructed by Kubernettes dialogue
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
- Deploy flannel.
```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
- Check the cluster state.
```
kubectl get pods --all-namespaces
```
### Complete the following step on the worker nodes only
---
- Run the join command you copied earlier
```
sudo kubeadm join <ip address>:<port> --token <token> --discovery-token-ca-cert-hash sha256:<sha>
```
- Then check your nodes from the master
```
kubectl get nodes
```
