# Kubernetes Replica Set

## Concepts
- Replica Sets are a set of pods that all shared the same labels
- The number of replicas defined in the spec will define how many duplicate pods are launched with this configuration.
- These pods will prefixed by the name from the spec with a random string appended afterwards
- These are the first step towards high availablity service but to truly get there we need to use deployments...

## Commands
- Create a replica set from a spec file
```
kubectl create -f ./replicas_example.yaml
```
- Show a replica set and it status
```
kubectl describe rs/<replica set name>
```
- Describe a single pod
```
kubectl describe pods <pod name>
```
- Changes the number of replicas in a replica set - this works for scaling up and down
```
kubectl scale rs/<replice set name> --replicas=<number of pods required>
```
- Delete a replica set 
```
kubectl delete rs/<replica set name>
```