# Network Policies

## What are Network Policies
* Network policies allow you to limit what network traffice is allowed to and from pods in your cluster
* By default all pods in the cluster can communicate with any other pod and reach out to any available IP
* As soon a pod is selected by a network policy it will now run on a whitelist basis and only allow traffic in according to the rules. 
* Ingress and Egress are split differently so if Ingress is specified but egress is not the pod can send traffic out to wherever it wants

## How to use Network Policies
* Network policies use the `networking.k8s.io/v1` apiVersion
* They are of the kind `NetworkPolicy`
* The spec of a network policy will contain the following:
  * `podSelector` to select pods that the policy applies to, can use `matchLabels`
  * `policyTypes` sets whether the policy governs incomming traffic (`Ingress`) or outgoing traffic (`Egress`)
  * `ingress` / `egress` containing a list of rules 
  * Rules contain the following
    * `from` / `to` selectors, can be multiple types of selectors and they can combine, podSelector and namespaceSelector can both be present (and logic)
      * podSelector: matches traffic from/to pods which match the selector
      * namespaceSelector: matches traffic from/to within namespaces which match the selector
      * ipBlock: specifies a cidr range of IP's that will match the fule. This is mostly used for external service. You can also specify exceptions to the range using `except`
    * `ports` to specify what protocol and port number traffic can move over 

## Examples
* Commands
  * `kubectl get networkpolicies`
  * `kubectl describe networkpolicy my-network-policy`
* This network policy allows pods with tag agg:MyApp to talk to all other pods with that tag on TCP:6379
```
apiVersion: netwroking.k8s.io./v1
kind: NetworkPolicy
metadata:
  name: my-network-policy
spec:
  podSelector:
    matchLabels:
      app: MyApp
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
      matchLabels:
        app: MyApp
    ports:
    - protocol: TCP
      port: 6379
```
* This policy applies to pods with label app: secure-app, it illows traffic to and from other pods with the label allow-access: "true" over TCP: 80
```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: my-network-policy
spec:
  podSelector:
    matchLabels:
      app: secure-app
  policyTypes:
  - Ingress
  - Egress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          allow-access: "true"
    ports:
    - protocol: TCP
      port: 80
  egress:
  - to:
    - podSelector:
        matchLabels:
          allow-access: "true"
    ports:
    - protocol: TCP
      port: 80
```


## Network Plugin
* The network plugin flannel does not support network policies so we will need to install a different plugin to use them
* Install canal network plugin
```
wget -O canal.yaml https://docs.projectcalico.org/v3.5/getting-started/kubernetes/installation/hosted/canal/canal.yaml

kubectl apply -f canal.yaml
```

:::info
Relevant documenation
* [network-policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
:::