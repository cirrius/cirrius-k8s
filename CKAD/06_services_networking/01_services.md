# Services

## What are Services
* Services create an abstraction layer which provides netwrok access to a dynamic set of pods
* It is most commonly used on a set of replica pods from a deployment
* The dyanmic nature, number of pods, pods may be replaced makes using IP addresses cumbersome
* Most services use a selector to determine which pods will recieve traffic through the service
* 

## How to use Services
* They have the kind `Service` and are part of the `v1` apiVersion
* The spec of a Service includes
  * type: this is a value specifying the behavior of the service
    * `ClusterIP`: (internal) the service is exposed within the cluster using an internal IP address. The service is also accesible using the cluster dns
    * `NodePort`: (external) the service is exposed externally via a port which listens on each node in the cluster
    * `LoadBalancer`: (cloud external) this only works if your cluster is set up to work with a cloud provider. The service is exposed through a load balancer that is created on the cloud platform
    * `ExternalName`: This takes an resource that is external to your cluster and allows you to map it to a service in the same way something inside the cluster would. This only sets up a DNS mapping it does not proxy traffic
  * selector: the key: value tag to use to determine which pods are going to a part of the service
  * ports: specifys how traffic will be proxied from the cluster to the pods includes:
    * protocol: TCP
    * port: the port coming into the cluster
    * targetPort: the port to send traffic to on the pod. If port and targetPort are the same tragetPort can be omitted

## Examples
* A simple service using ClusterIP to serve requests inside the cluster
```
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: ClusterIP
  selector:
    app: nginx
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 80
```

* The same service but using NodePort to expose the service externally. Note the `nodePort` must be between 30000-32767
```
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: NodePort
  selector:
    app: nginx
  ports:
  - protocol: TCP
    port: 80
    nodePort: 30080
```

:::info
Relevant documenation
* [services-networking/service](https://kubernetes.io/docs/concepts/services-networking/service/)<br>
* [expose-intro](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/)
:::