# Monitoring Applications

## How to monitor applications
* When working with a metrics-server you can use `kubectl top` to gather information about resource usage within the cluster
* You can get information for all pods in the default namespace `kubectl top pods`
* For a single pod `kubectl top pod {pod name}`
* For a different namespace `kubectl top pods -n {namespace}`
* For nodes `kubectl top nodes`


:::info
Relevant documenation
* [resource-usage-monitoring](https://kubernetes.io/docs/tasks/debug-application-cluster/resource-usage-monitoring/)
:::