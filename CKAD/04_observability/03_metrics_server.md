# Installing Metrics Server

## What is the metrix server
* This is not required for CKAD exam but is required for the next lesson
* The Kubernetes metrics server provides an API which allows you to access data about your pods and nodes
* Such as CPU and memory usage

## How to 
* Install the server
```
cd ~/
git clone https://github.com/linuxacademy/metrics-server
cd ./metrics-server
git checkout k8s-v1.13
kubectl apply -f ~/metrics-server/deploy/1.8+/
```
* Check the metrics-server is up and running with `kubectl get --raw /apis/metrics.k8s.io/`

## Examples
* 


:::info
Relevant documenation
* [linux-academy-metrics-server-githuib](https://github.com/linuxacademy/metrics-server)
:::