# Debugging

## What are 
* In order to succesfully debug applications you need to do two steps
* Find the problem
* Fix the problem

## How to 
* If you don't know what namespace an error is in you can run `kubectl get pods --all-namespaces`
* Edit an object's spec in the default editor `kubectl edit {object type} {object name}`
* You can take the current config of an edited object from the cluster so it can be recereated later `kubectl get {object type} {object name} -o yaml`
* This output will be very verbose as the export flags has been deprecated

:::info
Relevant documenation
* [debug-application](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-application/)<br>
* [debug-pod-replication-controller](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-pod-replication-controller/)<br>
* [debug-service](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/)
:::