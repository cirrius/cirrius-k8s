# Liveness & Readiness Probes

## What are liveness & readiness probes
* Probes allow you to customize how Kubernetes determines the status of your containers
* Liveness probes indicate whether the container is running properly, this governs when the cluster will automatically stop or restart the container
* Readiness probes indicate whether the container is ready to service requests and governs whether requests will be forwarded to the pod.
* Liveness and readines probes determine container status by doing things like running a command or making an http request (tests)

## How to create probe
* livenessProbe & readinessProbe are added to the container spec within a pod
* There are different types of probe that can be used
  * An `exec` probe allows you to run commands within the command to determine status, commands to be run are specified as a lit
  * An `httpGet` probe makes a http request **to the pod**, the `path` and `port` need to specified 
  * 
* initialDelaySeconds stops the probe being ran for a period of time after creation if we know the container will not be ready
* periodSeconds determines how frequently the probe will be run

## Examples
* A pod with a liveness probe
```
apiVersion: v1
kind: Pod
metadata:
  name: my-liveness-pod
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', "echo Hello, Kubernetes! && sleep 3600"]
    livenessProbe:
      exec:
        command:
        - echo
        - testing
      initialDelaySeconds: 5
      periodSeconds: 5
```

* A pod with a readiness probe
```
apiVersion: v1
kind: Pod
metadata:
  name: my-readiness-pod
spec:
  containers:
  - name: myapp-container
    image: nginx
    readinessProbe:
      httpGet:
        path: /
        port: 80
      initialDelaySeconds: 5
      periodSeconds: 5
```


:::info
Relevant documenation
* [container-probes](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes)<br>
* [configure-liveness-readiness-probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/)
:::