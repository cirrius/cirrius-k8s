# Container Logging

## What is Container Logging
* A containers normal console output goes into the container log.
* You can access container logs using `kubectl logs` command
* If you are retreiving logs from a multi container pod, you need to specify the container name with the `-c` parameter
* 

## How to 
* Show a pods logs - `kubectl logs {pod name}`
* Show a specific container within a multi container pod - `kubectl logs {pod name} -c {container name}`
* Output a pods logs to a file - `kubectl logs {pod name} > {file name}.log`

## Examples
* This pod continous prints the date and time that can be used to view the logs
```
apiVersion: v1
kind: Pod
metadata:
  name: counter
spec:
  containers:
  - name: count
    image: busybox
    args: [/bin/sh, -c, 'i=0; while true; do echo "$i: $(date)"; i=$((i+1)); sleep 1; done']
```

:::info
Relevant documenation
* [logging](https://kubernetes.io/docs/concepts/cluster-administration/logging/)
:::