# Config Maps

## What is a Config Map
* A ConfigMap is a Kubernetes Object that store configuration data in a key: value format.
* This configuration data can then be used to configure software running in a container by referencing the config map.
* This could be used to run the some pods / replicas templates in dev & prd but deploy those with different settings in each environment.
* A ConfigMap needs a name to reference it later, and then all of its key: values created in the data: option
* ConfigMaps can be accesed from within a pod through either environment variables or mounting the config map as a volume.

## Creating a ConfigMap
A ConfigMap object
```
apiVersion: v1
kind: ConfigMap
metadata:
   name: my-config-map
data:
   myKey: myValue
   anotherKey: anotherValue
```

## Using a ConfigMap
A Pod passing a ConfigMap value as an environment variable
```
apiVersion: v1
kind: Pod
metadata:
  name: my-configmap-pod
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', "echo $(MY_VAR) && sleep 3600"]
    env:
    - name: MY_VAR
      valueFrom:
        configMapKeyRef:
          name: my-config-map
          key: myKey
```
---
It's also possible to pass ConfigMap data to containers, in the form of file using a mounted volume, like so:
```
apiVersion: v1
kind: Pod
metadata:
  name: my-configmap-volume-pod
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', "echo $(cat /etc/config/myKey) && sleep 3600"]
    volumeMounts:
      - name: config-volume
        mountPath: /etc/config
  volumes:
    - name: config-volume
      configMap:
        name: my-config-map
```
* Each top level key in the ConfigMap becomes a file in the mounted volume and the value of that file will be the value of that key.
---
Multi line files can be written with a config map as follows:
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: candy-service-config
data:
  candy.cfg: |-
    candy.peppermint.power=100000000
    candy.nougat-armor.strength=10
```
And then consumed within a pod, this will write the contents of candy.cfg into `/etc/candy-service/candy.cfg`
```
apiVersion: v1
kind: Pod
metadata:
  name: candy-service
spec:
  containers:
  - name: candy-container
    image: linuxacademycontent/candy-service:1
    volumeMounts:
    - name: config-volume
      mountPath: /etc/candy-service
  volumes:
  - name: config-volume
    configMap: 
      name: candy-service-config
```


## kubectl commands

* `kubectl logs ${pod name}` shows the log from the pod specified
* `kubectl exec ${pod name} ${commands}` runs a command on the pod specified and brings back the stdout value

:::info
Relevant documenation
* [configure-pod-configmap](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/)  
:::