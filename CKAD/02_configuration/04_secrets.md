# Secrets

## What are Kubernetes Secrets
* Secrets are pieces of sensitive information stored within the Kubernetes cluster.
* They might be things such as tokens, passwords or keys.
* If a container needs a sensitive piece of information such as a password it is more secure to store it as a secret than storing it in a pod spec or in the container itself.

## Example Secret
* Yaml files may not be the best way to create a secret as the file might be left behind
* Multiple key: values can be stored within a single secret
```
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
stringData:
  myKey: myPassword
```
Consuming a secret within a pod
```
apiVersion: v1
kind: Pod
metadata:
  name: my-secret-pod
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', "echo Hello, Kubernetes! && sleep 3600"]
    env:
    - name: MY_PASSWORD
      valueFrom:
        secretKeyRef:
          name: my-secret
          key: myKey
```

:::info
Relevant documenation
* [secrets](https://kubernetes.io/docs/concepts/configuration/secret/)  
:::