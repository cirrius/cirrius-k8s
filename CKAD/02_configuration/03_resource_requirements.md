# Resource Requirements

## Specifying Resource Requirements
* Kubernetes allows us to specify the resource requirements of a container in the pod spec.
* A containers memory and CPU requirements are defined in terms of two measures: resource request and limits.
* **Resource request**: The amount of resoure necessary to run a container
  * A pod will only be run on a node that has enough resources to run the pods container (minimum resources)
* **Resource Limit**: A maximum value for the resource usage of a container (maximum resources)
  * If a resource limit is breached Kubernetes will likely restart that container
* **Memory**: is measured in bytes. `64Mi` means 64 Mebibytes
* **CPU**: is measured in cores. `250m` means 250 milliCPU's (250 one thousandth's of a core) or 0.25 CPU Cores

## Resource Requirements Example
```
apiVersion: v1
kind: Pod
metadata:
  name: my-resource-pod
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```

To view the Resource Request & Limits use `kubectl describe pod ${pod name}`
:::info
Relevant documenation
* [resource-requests-and-limits-of-pod-and-container](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#resource-requests-and-limits-of-pod-and-container)  
:::