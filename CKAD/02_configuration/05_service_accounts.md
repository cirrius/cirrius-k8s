# Service Accounts

## What are service accounts
* Kubernetes Service Accounts allow containers running in pods to access the Kubernetes API.
* Some applications may need to interact with the cluster itself for automation or other purposes.
* Service Accounts provide a way to let them do that with limited permission scopes.

## Using a service account
A service account can be used in a pod by specifying the `serviceAccountName` key within the pod spec.
```
apiVersion: v1
kind: Pod
metadata:
  name: my-serviceaccount-pod
spec:
  serviceAccountName: my-serviceaccount
  container:
    - name: myapp-container
      image: busybox
      command: ['sh', '-c',"echo Hello, Kubernetes! && sleep 3600"]
```
## Creating a service account
A simple demonstration service account - configuring Service Account isn't required for the CKAD exam.
```
kubectl create serviceaccount my-serviceaccount
```

:::info
Relevant documenation
* [service accounts admin](https://kubernetes.io/docs/reference/access-authn-authz/service-accounts-admin/)<br>
* [configure service account](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/)  
:::