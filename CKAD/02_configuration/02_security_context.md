# Security Context

## What is a security context
* A pod's securityContext define privilege and access control settings for a pod and the containers within it.
* If a container needs special operating system level permission we can provide them using the security context.
* The securityContext is defined within the pod spec:
* 

## Using a Security Context
The securityContext belows allows the container to run as specific user-id on the underlying operating system and with a specific group (This assumes that all of these users and groups are available on the worker nodes).
```
apiVersion: v1
kind: Pod
metadata:
  name: my-securitycontext-pod
spec:
  securityContext:
    runAsUser: 2000
    fsGroup: 3000
  containers:
    - name: myapp-container
      image: busybox
      command: ['sh', '-c' "cat /message/message.txt && sleep 3600"]
```
* This seems like a terrible idea, pods are now stateful based on the worker node they are deployed onto 🤮

## SecurityContext walkthrough
* Containers by default will be running as the root user - this means they will have full access to the volume mounted above
:::info
Relevant documenation
* [security-context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)  
:::
