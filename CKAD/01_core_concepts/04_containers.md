# Basic Container Configuration


## Custom Commands
* When defining a pod spec the command that will be used to run the container can be specifed. This will overide the default command specified by the container image.
* The command option in the pod spec allows this to be done

```
apiVersion: v1
kind: Pod
metadata:
  name: my-command-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['echo']
  restartPolicy: Never
```
* The pod spec above finishes almost immediately and because of the no-restart policy will not be replaced by Kuberenetes.

## Custom Arguments
* In addition to commands you can also specify arguments to that command in much the same way
```
apiVersion: v1
kind: Pod
metadata:
  name: my-args-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['echo']
    args: ['This is my custom argument']
  restartPolicy: Never
```

## Container Port
* If another pod or external to the cluster entity / service needs to connect with a pod it will need to have a containerPort opened to do so
* To do this the ports: -continerPort option allows this to be specified
```
apiVersion: v1
kind: Pod
metadata:
  name: my-containerport-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: nginx
    ports:
    - containerPort: 80
```

:::info
Relevant documenation
* [define-command-argument-container](https://kubernetes.io/docs/tasks/inject-data-application/define-command-argument-container/)  
:::