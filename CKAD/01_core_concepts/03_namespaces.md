# Namespaces

## What are Namespaces
Namespaces provide a way to keep your objects organisd within the cluster.<br>
Every objecty belongs to a name.<br>
Any time you don't specify the namespace you are using the `default` namespace.<br>
The backed Kubernetes pods are contained within then `kube-system` namespace.<br>
When creating an object in YAML the namespace can be specified in
```
metadata: 
    namespace: ${namespace name}
```
If an object is not in the default namespace you will to specify the namespace it is within
```
kubectl get pods -n ${namespace name}
```

## Namespace Commands
* Show all namespaces
```
kubectl get namespaces
```
* Create a new namespace
```
kubectl create ns ${namespace name} / kubectl create namespace ${namespace name}
```
* 

:::info
[Relevant documenation (namespaces)](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) 
:::