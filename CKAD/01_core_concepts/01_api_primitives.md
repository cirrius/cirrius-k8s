# API Primatives

## What are API Primatives 
API Primatives are data objects that represent the state of the cluster, the nodes in the cluster and the applications running on it.<br><br>
They can include but are not limited to:
- Pod - where the containers run
- Node - the servers the cluster runs on
- Service - a dynamic load balancer for multiple pods
- Service Account

<br>

To list the object types available to the cluster use `kubectl api-resources`

Every object in a cluster has two properties
- spec: the desired state of that object
- status: the actual current state of that object

Kubernetes objects are often represented in the yaml format so you can create an object by providing the cluster with yaml defining the object and it's spec.

- You can get lists of objects using `kubectl get ${object type}`
- You can get the yaml representation of an object using `kubectl get ${object type} ${object name} -o yaml`
- - You can get information about an objects spec and stats using `kubectl describe ${object type} ${object name}`

:::info
[Relevant documenation (kubernetes objects)](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/) 
:::