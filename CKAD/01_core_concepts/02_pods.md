# Pods

## What are Pods
The basic building block of Kuberenetes - the point of Kubernetes is to run containers and pods are where these are run within kubernetes.<br>
A pod contains one or more containers and a set of resources shared by those containers.<br>
All continers managed by a Kubernetes cluster are part of a pod.<br>

## A Basic Pod YAML Format
* apiVersion - what version of the Kubernetes API the specification is compatibile with.
* kind - Pod obviously
* metadata -
  * name - the name of the pod
  * labels - key: value pairs that can be used keeps your objects organised and run automation on 
* spec -
  * containers - this is a list of containers to run within the pod
    * name - the name of the container
    * image - the image to run in the container
    * command - specifies the command that will be run in the container, optional this could be specified by the image itself.

```
apiVersion: v1
kind: Pod
metadata:
  name: my-ns-pod
  namespace: my-ns
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox
    command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
```

## Pod Commands
* Show all pods
```
kubectl get pods
```
* Create a pod from a YAML file (or any other object)
```
kubectl create -f ${specification}.yaml
```
* To update a pod from YAML
```
kubectl apply -f ${specification}.yaml
```
* To update a pod from within a text editor
```
kubectl edit pod ${pod name}
```
* To delete a pod
```
kubectl delete pod ${pod name}
```

* To enter an interactive shell session within a pod
```
kubectl exec --stdin --tty ${pod name} -- /bin/bash
```

:::info
[Relevant documenation (pods)](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/) 
:::
