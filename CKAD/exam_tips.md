# Exam Tips

* `source <(kubectl completion bash)` running this command enables autocompletion for kubectl
* Skip hard questions and come back to them later
* Login as root to save time `sudo -i`
* Pay attention to what cluster you are using it will be written at the start of the question
* Use the `kubectl config use-context {cluster name}` to swtich cluster contexts