# Jobs and Cron Jobs

## What are jobs
* Jobs are an abstraction on top of pods for one off or scheduled workloads
* Pods are meant to be run continously
* Jobs are meant to be run and then finish
* When a job is finished its containers will exit and the pods will enter the completed step
* The job will still exist as a kubernetes object but the pods will be shut down

## How to use jobs
* Jobs are part of `batch/v1` apiVersion
* kind is `Job`
* The spec of a job is a template which containes a pod spec as if it were a pod
* A job spec can also contain a backoffLimit which will stop a job if fails a certain amount of times

## Examples
* This job calculates the first 2000 digits of pi before exiting
```
apiVersion: batch/v1
kind: Job
metadata:
  name: pi
spec:
  template:
    spec:
      containers:
      - name: pi
        image: perl
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: Never
  backoffLimit: 4
```

## What are cron jobs
* CronJob's are just jobs that run on a shedule as per a cron job with Linux

## How to use CrobJobs
* Jobs are part of `batch/v1beta1` apiVersion
* They are of kind `CronJob`
* The spec of the CronJob will have a `schedule` in the format `minute hour dayOfMonth month dayOfWeek`
* The spec will also have a `jobTemplate` is where we provide a job spec as per a job above
* To view current cronJobs run `kubectl get cronjob`

## Examples
* A CronJob running every minute
```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            args:
            - /bin/sh
            - -c
            - date; echo Hello from the Ku
```

:::info
Relevant documenation
* [jobs-run-to-completion](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/)<br>
* [controllers/cron-jobs](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)<br>
* [automated-tasks-with-cron-jobs](https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/)
:::