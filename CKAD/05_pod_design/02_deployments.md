# Deployments

## What are deployments
* Provides a way to declaratively manage a dynamic set of replica pods
* A deployment defines a desired state for the replica pods
* The cluster will constantly work to maintain that desired state, creating removing and modifying replica pods accordingly

## How to use deployments
* Deployments are part of the app apiVersion this should be reflected in the yaml file
* spec.replicas controls the number of replica pods that will be created
* spec.template will define the pod that will be launched, it follows the format of a pod spec
* spec.selector the deployment will manage all pods whose labels match this selector 

## Examples
* A simple nginx deployment
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

:::info
Relevant documenation
* [deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
:::
