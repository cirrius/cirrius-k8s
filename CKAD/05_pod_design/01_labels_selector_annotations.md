# Labels, Selecttors and Annotations

## What are labels
* Labels are key: value pairs attached to Kubernetes objects
* They are used for identifying attributes of objects which can in turn be used to select and group objects
* Production pods may be labeled with an envrionment of `prd` and development with `dev`

## How to use labels
* Labels can be set in the metadata of an object and key: value's can be defined by the developer
```
apiVersion: v1
kind: Pod
metadata:
  name: my-production-label-pod
  labels:
    app: my-app
    environment: production
spec:
  containers:
  - name: nginx
    image: nginx
```
* A pods labels can be show with `kubectl get pods --show-labels`
* Or `kubectl describe pod {pod name}`

## What are selectors
* Selectors are used for identifying and selecting a specific group of objects using their labels
* These are used by many other advanced features (services, replica sets) that will be covered later in the course

## Using selectors
* They can also be used by `kubectl get pods -l {key}={value}`
* Inequality can also be used `kubectl get pods -l {key}!={value}`
* Values from an array can be selected `kubectl get pods -l '{key} in ({value1},{value2},...)'`
* Selectors can also be chained `kubectl get pods -l {key1}={value1},{key2}={value2}`

## What are annotations
* Annotations are very similar to labels and are used for storing metadata about an object
* They cannot be used to select or group objects though
* External tools can read, write and interact with annotations


## Using annotations
* They are defined within metadata very much like labels
* A simple pod with some annotations
```
apiVersion: v1
kind: Pod
metadata:
  name: my-annotation-pod
  annotations:
    owner: terry@linuxacademy.com
    git-commit: bdab0c6
spec:
  containers:
  - name: nginx
    image: nginx
```
* Annotations can be viewed using `kubectl describe`

:::info
Relevant documenation
* [labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)<br>
* [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/)
:::