# Rolling Updates and Rollbacks

## What are rolling updates
* Rolling updates provide a way to updatea deployment to a new container version by graudaully updating replicas so there is no downtime.
* The rolling update creates pods with a new container version while keeping the old version live
* Only when the new pods are ready (deployed and passed their health checks) will it start to remove the old pods


## How to start a rolling update
* `kubectl set image deployment/{deployment name} {container name}={new image name} --record`
* The rolling update targets a specfic deployment and container and allows the image to be updated
* The record flag allows you to run a rollback later if you need to

## What are rollbacks
* Rollbacks allows us to revert to a previous state
* If a rolling update were to break something we can quikcly recover by using a rollback
* 

## How to use a rollback
* Get a list of previous rolling updates `kubectl rollout history deployment/{deployment name}`
* The revision flag will give more information on a specifc revision number `kubectl rollout history deployment/{deployment name} --revision={revision number}`
* Rollback the last revision `kubectl rollout undo deployment/{deployment name}`
* Rollback to a specific revision number `kubectl rollout undo deployment/{deployment name} --to-revision={revision number}`

## What are rolling update strategies
* maxSurge is the maximum percentage of additional pods that can be created during an update (a higher number here will cause a faster update with additonal resource consumption)
* maxUnavailable is the maximum percentages of pods that can made unavailble during an update (a higher number here will cause a slower update with less loss of avialability)


:::info
Relevant documenation
* [updating-a-deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#updating-a-deployment)<br>
* [rolling-back-a-deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#rolling-back-a-deployment)
:::