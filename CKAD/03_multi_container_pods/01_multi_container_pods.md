# Multi Container Pods 

## What are multi-continer pods 
* Multi-continer pods are simply pods with more than one container that all work together as a single unit.
* It is often a good idea to keep containers separate by keeping them in their own pods but there are cases where multi-continer pods can be beneficial.
* To create a multi-continer pod simply list multiple containers in the pod spec

## Basic Example
```
apiVersion: v1
kind: Pod
metadata:
  name: multi-container-pod
spec:
  containers:
  - name: nginx
    image: nginx:1.15.8
    ports:
    - containerPort: 80
  - name: busybox-sidecar
    image: busybox
    command: ['sh', '-c', 'while true; do sleep 30; done;']
```

## How can container interact within a pod
* Shared network: the two containers function as if they were on the same system and if container 1 is listening on port 1234, container 2 can reach it through `localhost:1234`
* Shared Storage Volumes: storage volumes are created at the pod level, and two containers can mount the same volume and communicate through files saved to disk
* Shared Process Namespace: if `shareProcessNamespace: true` is set in the pod spec than the two containers processes can interact with and signal one another processess.

## Design Patterns
### Sidecar Pod
* This pattern is a very general pattern but it uses a sidecar container to enhance or add functionality to the the main container. 
* This could be something that syncs files from a Git repository to the file system this would automatically deploy new version of a web application without having to redeploy a container.
![](https://i.imgur.com/StXnXLT.png)

### Ambassador Pod
* The ambassador pattern use an ambassador container to accept netwrok traffic and pass it on to the main container.
* An example of this pattern would be for the ambassador to listen on one specific port and pass it on to another port (if your application is not easy to change what port it listens on)
![](https://i.imgur.com/nAxQJtM.png)

### Adapter Pod
* The adapter pattern is all about change and transform the output of the main container before sending it elsewhere.
* One of the most common use cases for this pattern is an adapter that formats and decorates log output from the main container.
![](https://i.imgur.com/OxZa9fX.png)

:::info
Relevant documenation
* [using-a-sidecar-container-with-the-logging-agent](https://kubernetes.io/docs/concepts/cluster-administration/logging/#using-a-sidecar-container-with-the-logging-agent)<br>
* [communicate-containers-same-pod-shared-volume](https://kubernetes.io/docs/tasks/access-application-cluster/communicate-containers-same-pod-shared-volume/)<br>
* [the-distributed-system-toolkit-patterns](https://kubernetes.io/blog/2015/06/the-distributed-system-toolkit-patterns/)
:::