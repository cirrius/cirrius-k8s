# Commands to restart Local Kubernetes Cluster after shutdown

```
swapoff -a
kubeadm reset
kubeadm init --pod-network-cidr=10.244.0.0/16

# Copy on screen commands as regular user

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
